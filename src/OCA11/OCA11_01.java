package OCA11;

class OCA11_01 {
	String str = "duke";

	OCA11_01(String s) {
		str = s;
	}

	void print() {
		System.out.println(str);
	}

	public static void main(String[] args) {
		new OCA11_01("java").print();
	}

}
