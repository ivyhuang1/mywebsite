package OCA11;

class OCA11_11 {
	double radius;
	public double area;
	public OCA11_11(double r) {
		radius =r;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double r) {
		radius = r;
	}
	
	public double getArea() {
		return area;
	}
	
	class Test {
		OCA11_11 c = new OCA11_11(9.1);
		c.area = Math.PI*c.getRadius()*c.getRadius();
	}


}
