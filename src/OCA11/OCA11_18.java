package OCA11;

class OCA11_18 {

	public int sum;

	OCA11_18(int sum) {
		this.sum = sum;
	}

	public int getSum() {
		return sum;
	}

	public void modifyAmount(int x) {
		sum += x;
	}

	public static void main(String[] args) {
		OCA11_18 act = new OCA11_18((int)(Math.random()*1000));
		act.modifyAmount(-act.sum);
		System.out.println(act.getSum());
	}

}



