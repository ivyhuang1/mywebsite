package Part1;

public class T01 {
	 
    public int amount;
 
    public T01(int amount) {
        this.amount = amount;
    }
 
    public int getAmount() {
        return amount;
    }
 
    public void changeAmount(int x) {
        amount += x;
    }
    
    public static void main(String[] args){
    	T01 acct = new T01((int)(Math.random() * 1000));

    	acct.changeAmount(-acct.getAmount());

    	System.out.println(acct.getAmount());
	}
}
