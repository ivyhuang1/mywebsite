package TEST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chenlong on 2017/3/16.
 */
public class ShipPosition {
    //初始化一個需要使用的List
    public static List<ShipPosition> init(){
        List<ShipPosition> shipInfoList=new ArrayList<ShipPosition>();
        //目標1
        ShipPosition shipPositon1=new ShipPosition(1,60,120,60);
        shipInfoList.add(shipPositon1);
        //目標2的軌跡
        ShipPosition shipPositon2=new ShipPosition(2,58,121,60);
        shipInfoList.add(shipPositon2);
        ShipPosition shipPositon6=new ShipPosition(2,58,122,60);
        shipInfoList.add(shipPositon6);
        //目標3的軌跡
        ShipPosition shipPositon3=new ShipPosition(3,59,122,60);
        shipInfoList.add(shipPositon3);
        ShipPosition shipPositon4=new ShipPosition(3,59,123,60);
        shipInfoList.add(shipPositon4);
        ShipPosition shipPositon5=new ShipPosition(3,59,124,60);
        shipInfoList.add(shipPositon5);
        return shipInfoList;
    }

    public static void main(String[] args){
        /**
         * 現在需要對不同的目標放在不同的列表裡面,怎麼實現呢?
         * 感覺使用map和List都差一點,於是使用Map巢狀List,並且寫函式實現:有這個目標就新增進以目標ID為key,船舶位置列表為value的Map中,沒有就新建key和列表
         */
        //初始化一個需要使用的List
        List<ShipPosition> shipPositonList=init();

        int ID;
        Map<String,ArrayList<ShipPosition>> map=new HashMap<String,ArrayList<ShipPosition>>();
        for(int i=0;i<shipPositonList.size();i++){
            ID=shipPositonList.get(i).getId();
            //呼叫該方法生成以ID為Key,以目標位置列表為List的Map
            putAdd(map,Integer.toString(ID),shipPositonList.get(i));
        }

        //遍歷輸出分類後的結果
        Dump(map);
    }

    /**
     * 有這個目標就新增進以目標ID為key,船舶位置列表為value的Map中,沒有就新建key和列表函式
     * @param map
     * @param key
     * @param shipPositon
     * @return
     */
    public static Map<String,ArrayList<ShipPosition>> putAdd(Map<String,ArrayList<ShipPosition>> map, String key, ShipPosition shipPositon)
    {
        if(!map.containsKey(key)){
            map.put(key, new ArrayList<ShipPosition>());
        }
        map.get(key).add(shipPositon);
        return map;
    }

    /**
     * 列印結果
     * @param map
     */
    public static void Dump(Map<String,ArrayList<ShipPosition>> map){
        for(Map.Entry<String,ArrayList<ShipPosition>> entry:map.entrySet()){
            System.out.println("Key:"+entry.getKey());
            for(ShipPosition shipPosition:entry.getValue()){
                System.out.println("Value-List:"+shipPosition.getId()+","+shipPosition.getSpeed()+","+shipPosition.getLongitude()+","+shipPosition.getLatitude());
            }
        }
    }
}