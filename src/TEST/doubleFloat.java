package TEST;

public class doubleFloat {

	Integer a = Integer.valueOf("1");

	public static void main(String[] nums) {
		double d1 = 5f; // p1
		double d2 = 5.0; // p2
		float f1 = 5f; // p3
		
//		double d = new double(1_000_000.00);//X
		double d = new Double(1_000_000.00);//ok
//		Double d = new double(1_00_000.00);//X
		//float f2 = 5.0; // p4 這樣會報錯因為java會以為是double
		//要就這樣寫 float f2 = 5.0f; 
		//或是這樣float f2 = (float)5.0; 
		
		
//		1.float是單精度浮點數，內存分配4個字節，佔32位，有效小數位6-7位
//		double是雙精度浮點數，內存分配8個字節，佔64位，有效小數位15位
		
//		2.java中默認聲明的小數是double類型的，如double d=4.0
//		如果聲明： float x = 4.0則會報錯，需要如下寫法：float x = 4.0f或者float x = (float)4.0
//		其中4.0f後面的f只是為了區別double，並不代表任何數字上的意義

//		3.對編程人員來而，double 和float 的區別是double精度高，但double消耗內存是float的兩倍，且double的運算速度較float稍慢。
		
	}
}
