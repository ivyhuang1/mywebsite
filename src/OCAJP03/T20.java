package OCAJP03;

public class T20 {

	public static void main(String[] args) {
		int leaders = 10 * (2 + (1 + 2 / 5);
		int followers = leaders * 2;
		System.out.print(leaders + followers < 10 ? "Too few" : "Too many");
	}

}
