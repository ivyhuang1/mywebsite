package OCAJP02;

public class T49 {

	public void play() {
		System.out.print("play-");
	}

	public void finalize() {
		System.out.print("clean-");
	}

	public static void main(String[] args) {
		T49 car = new T49();
		car.play();
		System.gc();
		T49 doll = new T49();
		doll.play();
	}

}
