package OCAJP02;

public class T46 {

	public T46 name;

	public static void main(String... args) {
		T46 a = new T46();
		T46 b = new T46();
		a.name = b;
		b = null;
		T46 zoe = new T46();
		a.name = zoe;
		zoe = null;
	}

}
