package OCA10;

/**
 * @author IVY
 * 第十章第六題
 */

public class OCA10_06 {
	
	static void method(int i) {
		i += 8;
	}
	public static void main(String[] args) {
		int j=12;
		method(j);
		System.out.println(j);
	}

}
