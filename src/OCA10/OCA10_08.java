package OCA10;

/**
 * @author IVY
 * 第十章第八題
 * 靜態方法：https://openhome.cc/Gossip/Java/Static.html
 * 在類別設計裡面，如果把屬性或方法加上static修飾詞。則該屬性或方法，使用時就不需要再透過物件生成，
 * 亦即直接使用類別，就能呼叫static方法和變數。
 */

public class OCA10_08 {

	public static void m1() {
		m2();
		OCA10_08.m2();
		m3();  //靜態方法不可以直接呼叫非靜態方法和成員，也不能使用this關鍵字。
		OCA10_08.m4();
	}
	
	public static void m2() {}
	public  void m3() {
		m1();
		OCA10_08.m2();
		m4();
		OCA10_08.m4();
	}
	public void m4() {
	}
}
