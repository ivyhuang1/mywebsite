package OCA10;

class StaticTest {
	int ns;
	static int s;

	StaticTest(int ns) {
		if (s < ns) {
			s = ns;
			this.ns = ns;
		}
	}

	void doPrint() {
		System.out.println("ns = " + ns + ", s =" + s + ";\t");
	}
}

public class OCA10_13 {
	public static void main(String[] args) {
		StaticTest a1 = new StaticTest(50);
		StaticTest a2 = new StaticTest(125);
		StaticTest a3 = new StaticTest(110);
		a1.doPrint();
		a2.doPrint();
		a3.doPrint();
	}

}
