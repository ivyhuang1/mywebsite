package OCA13;

public class OCA13_26 {

	void read(int c) throws Exception{
		System.out.println("Reading ");
	}
	
	void check(int c) throws RuntimeException{ //line n1
		System.out.println("Checking ");
	}
	public static void main(String[] args) throws Exception {
		OCA13_26 ex = new OCA13_26();
		int c = 99999;
		ex.check(c); // line n2
		ex.read(c);  // line n3
	}
	

}
