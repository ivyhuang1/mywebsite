package OCA13;


class CustomizedOutOfBoundsException extends IndexOutOfBoundsException{}

public class OCA13_19{
	void verify(int marks) throws IndexOutOfBoundsException{
		if(marks>90) {
			throw new CustomizedOutOfBoundsException();
		}
		if(marks>40) {
			System.out.println("OK");
		}else {
			System.out.println("NG");
		}
	}

	public static void main(String[] args) {
		int marks = Integer.parseInt(args[1]);
		try {
			new OCA13_19().verify(marks);
		}catch(Exception e) {
			System.out.println(e.getClass());
		}
	}
}


