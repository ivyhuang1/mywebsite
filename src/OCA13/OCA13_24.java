package OCA13;

import java.awt.color.CMMException;
import java.io.IOException;

public class OCA13_24 {

	public static void main(String[] args) {
		try {
			methodX();
		}catch(CMMException e) {
			System.out.println("M");
		}
	}
	
	public static void methodX() {
		try {
			throw Math.random() < 0.7 ? new Exception(): new RuntimeException();
		}catch(RuntimeException e) {
			System.out.println("X");
		} catch (Exception e) {
			System.out.println("IO");
			e.printStackTrace();
		}
	}

		
}
