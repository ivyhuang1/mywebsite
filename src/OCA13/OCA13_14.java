package OCA13;

public class OCA13_14 {

	public static void main(String[] args) {
		OCA13_14 t = new OCA13_14();
		try {
			t.doOne();
			t.doMore();
		}catch(Exception e) {
			System.out.println("Caught"+e);
		}
	}
	
	public void doMore() throws Exception{
		throw new Error("Error");
	}
	
	public void doOne() throws Exception{
		throw new RuntimeException("Exception");
	}

}
